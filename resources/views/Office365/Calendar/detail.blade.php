@extends('theme.app')

@section('title')
    Calendar detail
@endsection

@section('content')
    <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">
            <h2>
                View Event
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">


                <h3>
                    <span class="font-normal">Subject: </span> {{ $event->getSubject() }}
                </h3>
                <h5>
                    <span class="float-right font-normal">Start: </span>{{ (new DateTime($event->getStart()->getDateTime()))->format(DATE_RFC2822) }}
                    <span class="float-right font-normal">End: </span> {{ (new DateTime($event->getEnd()->getDateTime()))->format(DATE_RFC822) }}
                </h5>
            </div>
        </div>
        <div class="mail-box">


            <div class="mail-body">
                {!! $eventBody !!}
            </div>
        </div>
    </div>
@endsection