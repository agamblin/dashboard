@extends('theme.app')

@section('title')
    Calendar
@endsection

@section('content')
    <div class="col-lg-9">
        <div class="mail-box-header">
        <h2>
            Calendar
        </h2>
        <div class="space-20"></div>
        <form method="GET" action="/office/mail/search/" class="float-right mail-search">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" class="form-control form-control-sm" name="search" placeholder="Search event">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Search
                    </button>
                </div>
            </div>
        </form>
        <div class="mail-tools tooltip-demo m-t-md">
            <div class="btn-group float-right">
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

            </div>
            <a href="{{ route('calendar') }}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark as read"><i class="fa fa-eye"></i> </button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark as important"><i class="fa fa-exclamation"></i> </button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>

        </div>
    </div>
        <div class="mail-box">
        <table class="table table-hover table-mail">
            <tbody>
            @foreach($events as $event)
                    <tr class="unread">
                        <td class="check-mail">
                            <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                        </td>
                        <td class="mail-subject"><a href="{{  route('calendar.detail', [ $event->getId() ])}}"> <h3>{{ $event->getSubject() }}</h3></a></td>
                        <td class="text-right mail-date"> Start: {{ (new DateTime($event->getStart()->getDateTime()))->format(DATE_RFC2822) }}</td>
                        <td class="text-right mail-date"> End: {{ (new DateTime($event->getEnd()->getDateTime()))->format(DATE_RFC822) }}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection
