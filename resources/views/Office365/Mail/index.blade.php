@extends('theme.app')

@section('title')
    Mail
@endsection

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-2">
                <div class="ibox ">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="{{ route('mail.create') }}">Compose Mail</a>
                            <div class="space-25"></div>
                            <h5>Folders</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="{{ route('mail') }}"> <i class="fa fa-inbox "></i> Inbox <span class="label label-warning float-right">{{ $unread }}</span> </a></li>
                                <li><a href="{{ route('mail.create') }}"> <i class="fa fa-envelope-o"></i> Send Mail</a></li>
                                <li><a href="#"> <i class="fa fa-certificate"></i> Important</a></li>
                                <li><a href="#"> <i class="fa fa-file-text-o"></i> Drafts <span class="label label-danger float-right">2</span></a></li>
                                <li><a href="#"> <i class="fa fa-trash-o"></i> Trash</a></li>
                            </ul>
                            <h5>Categories</h5>
                            <ul class="category-list" style="padding: 0">
                                <li><a href="#"> <i class="fa fa-circle text-navy"></i> Taker </a></li>
                                <li><a href="#"> <i class="fa fa-circle text-danger"></i> Documents</a></li>
                                <li><a href="#"> <i class="fa fa-circle text-primary"></i> Association</a></li>
                                <li><a href="#"> <i class="fa fa-circle text-info"></i> Perso</a></li>
                                <li><a href="#"> <i class="fa fa-circle text-warning"></i> Code</a></li>
                            </ul>

                            <h5 class="tag-title">Labels</h5>
                            <ul class="tag-list" style="padding: 0">
                                <li><a href=""><i class="fa fa-tag"></i> DevApp</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Mathematics</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> C++</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Haskell</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> react-native</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Laravel</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Part-time</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="mail-box-header">
                    <h2>
                        Inbox ({{ $unread }})
                    </h2>
                    <div class="space-20"></div>
                    <form method="GET" action="/office/mail/search/" class="float-right mail-search">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm" name="search" placeholder="Search email">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="mail-tools tooltip-demo m-t-md">
                        <div class="btn-group float-right">
                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                        </div>
                        <a href="{{ route('mail') }}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</a>
                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark as read"><i class="fa fa-eye"></i> </button>
                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark as important"><i class="fa fa-exclamation"></i> </button>
                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>

                    </div>
                </div>
                <div class="mail-box">
                    <table class="table table-hover table-mail">
                        <tbody>
                        @foreach($messages as $message)
                            @if ($message->getIsRead() == false)
                                <tr class="unread">
                            @else
                                <tr class="read">
                            @endif
                            <td class="check-mail">
                                <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </td>
                            <td class="mail-contact"><a href="{{  route('mail.detail', [ $message->getId() ])}}"> {{ $message->getFrom()->getEmailAddress()->getName() }}</a></td>
                            <td class="mail-subject"><a href="{{  route('mail.detail', [ $message->getId() ])}}"> {{ $message->getSubject() }}</a></td>
                                    @if ($message->getHasAttachments() == true)
                            <td class=""><i class="fa fa-paperclip"></i></td>
                                        @else
                                        <td class=""></td>
                                    @endif
                            <td class="text-right mail-date"> {{ $message->getReceivedDateTime()->format(DATE_RFC2822) }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
@endsection