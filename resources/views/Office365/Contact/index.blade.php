@extends('theme.app')

@section('title')
    Contact
@endsection

@section('content')
    <div id="inbox" class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">Contacts</h1>
        </div>
        <div class="panel-body">
            Here are your first 10 contacts.
        </div>
        <div class="list-group">
            @foreach($contacts as $contact)
            <div class="list-group-item">
                <h3 class="list-group-item-heading"> {{$contact->getGivenName().' '.$contact->getSurname() }} </h3>
                <p class="list-group-item-heading"> {{ $contact->getEmailAddresses()[0]['address'] }} </p>
            </div>
            @endforeach
        </div>
    </div>
@endsection