<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - @yield('title') </title>


    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

</head>
<body>

  <!-- Wrapper-->
        @guest
        <div id="wrapper" class="gray-bg full-height">
            @else
        <div id="wrapper">
        @endguest

        <!-- Navigation -->
        @guest
        @else
        @include('theme.navigation')
        @endguest

        <!-- Page wraper -->
            @guest
                @else
        <div id="page-wrapper" class="gray-bg">
            @endguest

            <!-- Page wrapper -->
            @guest
            @else
            @include('theme.topnavbar')
            @endguest

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('theme.footer')

        @guest
            @else
        </div>
            @endguest
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

@section('scripts')
@show

</body>
</html>
