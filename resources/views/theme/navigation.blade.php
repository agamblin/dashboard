<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> <span class="text-muted text-xs block">Dashboard<b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href={{ route('logout') }}>Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    EDB
                </div>
                <li>
                    <a href="#">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Office365</span>
                        <span class="fa arrow">
                        </span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="false">
                        <li class="active">
                            <a href="/office/signin">
                                <i class="fa fa-superpowers"></i>
                                <span class="nav-label">Login</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ route('mail') }}">
                                <i class="fa fa-envelope-square"></i>
                                <span class="nav-label">Mail</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ route('calendar') }}">
                                <i class="fa fa-calendar"></i>
                                <span class="nav-label">Calendar</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ route('contacts') }}">
                                <i class="fa fa-vcard-o"></i>
                                <span class="nav-label">Contacts</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li><a href="{{ route('youtube', ["us"]) }}">
                    <i class="fa fa-caret-square-o-right"></i>
                    <span class="nav-label">YouTube</span>
                    </a>
                </li>
                <li class="{{ Request::is(['users', 'users/*']) ? 'active' : '' }}"><a href="{{ route('user.index') }}">
                    <i class="fa fa-user-o"></i>
                    <span class="nav-label">Users</span>
                    </a>
                </li>

        </ul>

    </div>
</nav>


