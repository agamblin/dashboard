@extends('theme.app')

@section('title')
    Login
@endsection
@section('content')
  <div class="middle-box text-center loginscreen animated fadeInDown">
      <div>
          <div>
              <h1 class="logo-name">EDB</h1>
          </div>
          <h3>Welcome to EpiDashboard</h3>
          <p>Focus your information intelligently. Perfectly designed with Inspinia.</p>
          <p>Log in to see it in action.</p>
          <form class="m-t" role="form" method="POST" action="{{ url('/login') }}">
              {!! csrf_field() !!}
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <input type="email" class="form-control" placeholder="Enter your email" name="email" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <input type="password" class="form-control" placeholder="Enter your password" name="password">
                  @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
              <button type="submit" class="btn btn-primary block full-width m-b">
                  <strong>Login</strong>
              </button>
              <a href="{{ url('/password/reset') }}">
                  <small>Forgot password ?</small>
              </a>
              <p class="text-muted text-center">
                  <small>Do not have an account ?</small>
              </p>
              <a class="btn btn-sm btn-white btn-block" href={{ route('register') }}>
                  Create an account
              </a>
          </form>
          <p class="m-t">
              <small>Designed for Epitech by Arthur GAMBLIN</small>
          </p>
      </div>
  </div>
@endsection
