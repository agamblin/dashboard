@extends('theme.app')

@section('title')
    Users
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>List of users</h5>
                </div>
                <div class="ibox-content">
                    @if(count($users))
                        <div class="table-responsive" style="border: none;">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Admin</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>#{{ $user->id }}</td>
                                        <td> {{ $user->name }}</td>
                                        <td> {{ $user->email }}</td>
                                        <td> {{ $user->admin }} </td>
                                        <td>
                                            <a href="{{ route('user.edit', [$user->id]) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('user.show', [$user->id]) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="destroy"><i class="fa fa-plus-square-o"></i></a>
                                            <td>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) !!}
                                                {!! Form::submit('X', ['class' => 'btn btn-danger btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'onclick' => 'return confirm(\'Vraiment supprimer cet utilisateur ?\')']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <p class="text-center">No user, click on <a class="text-navy" href="{{ route('user.create') }}">Add a user</a></p>
                        @endif
                </div>
            </div>
                <a class="pull-right btn btn-w-m btn-primary m-b-md btn-sm" href="{{ route('user.create') }}">Add a user</a>
        </div>
    </div>
@endsection