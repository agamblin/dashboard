@extends('theme.app')

@section('title')
    Youtube
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content text-center">
                        <h2>
                            <i class="fa fa-bar-chart-o"> </i> Search
                        </h2>
                        <small>Search videos</small>
                        <form method="GET" action="{{url('/youtube/search')}}" class="float-right search-form">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" class="form-control form-control-sm text-center" name="search" placeholder="Search video">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($videos as $video)
                @if (isset($video->id->videoId))
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Video</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-5">
                                <figure>
                                    <iframe src="http://www.youtube.com/embed/{{ $video->id->videoId }}" frameborder="0" allowfullscreen="" data-aspectratio="0.8211764705882353" style="width: 492.5px; height: 404.429px;"></iframe>
                                </figure>
                            </div>
                            <div class="col-lg-5">
                                <h4><strong>{{ $video->snippet->title }}</strong></h4>
                                <p><i class="fa fa-clock-o"></i> Uploaded on {{ (new DateTime($video->snippet->publishedAt))->format(DATE_RFC2822) }}</p>
                                <h5>
                                    @if (isset($video->snippet->tags))
                                        {{ $video->snippet->tags[0] }}
                                    @endif
                                </h5>
                                <p>
                                    {{ $video->snippet->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection