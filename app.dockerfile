FROM php:7.1.3-fpm

# install system dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libmcrypt-dev \
        mysql-client \
        libmagickwand-dev \
        openssl \
        zip unzip \
        git

# install php dependencies
RUN pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install mcrypt pdo_mysql


RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ARG env=dev

#
WORKDIR /var/www

# copy source code
COPY ./ /var/www

RUN mkdir -p bootstrap/cache
RUN mkdir -p storage/app/public
RUN mkdir -p storage/framework/cache
RUN mkdir -p storage/framework/sessions
RUN mkdir -p storage/framework/views
RUN mkdir -p storage/logs

# chmod 777 fix everything
RUN chmod -R 777 storage
RUN chmod -R 777 bootstrap/cache