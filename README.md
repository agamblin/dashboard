# PREREQUISITE
 *  Docker
 * Git
# INSTALLATION

### Step 1
Copy the file '.env.example' and name it as '.env' by typing the following command:
````bash
cp .env.example .env
````
### Step 2

Build the project typing the command:
```bash
docker-compose build
```
### Step 3
Up the service by typing:
```bash
docker-compose up
```
**Let it run !**
### Step 4
 In a new terminal,
 Install the laravel components by typing:
 ``` bash
 docker-compose exec app composer install
 ```

### Step 5
Add your key by typing the command:
 ``` bash
 docker-compose exec app php artisan key:generate
 ```


Go to localhost:8080 on your machine and enjoy !

###############################################################################

Developed by Arthur GAMBLIN for EPITECH PARIS