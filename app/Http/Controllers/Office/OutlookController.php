<?php

namespace App\Http\Controllers\Office;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class OutlookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return (redirect('home'));
    }

    public function mail()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $messageQueryParams = array (
            "\$select" => "subject,receivedDateTime,from,bodyPreview, id, isRead, hasAttachments",
            "\$orderby" => "receivedDateTime DESC",
            "\$top" => "50"
        );
        $getMessagesUrl = '/me/mailfolders/inbox/messages?'.http_build_query($messageQueryParams);
        $messages = $graph->createRequest('GET', $getMessagesUrl)
            ->setReturnType(Model\Message::class)
            ->execute();

        $count = 0;
        foreach ($messages as $message) {
            if ($message->getIsRead() == false) {
                $count = $count + 1;
            }
        }
        return view('Office365.Mail.index', array(
            'messages' => $messages,
            'unread' => $count
        ));
    }

    public function mailDetail($id)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());
        $getMessageUrl = '/me/mailfolders/inbox/messages/'.$id;
        $message = $graph->createRequest('GET', $getMessageUrl)
            ->setReturnType(Model\Message::class)
            ->execute();

        \Debugbar::addMessage($message);
        $msgBody = html_entity_decode($message->getBody()->getContent());
        return view('Office365.Mail.detail', array(
            'message' => $message,
            'msgBody' => $msgBody
        ));
    }

    public function mailSearch(Request $search)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $tokenCache = new \App\TokenStore\TokenCache;

        \Debugbar::addMessage($search->input('search'));
        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $messageQueryParams = array (
            "\$search" => "\"".$search->input('search')."\"",
            "\$select" => "subject,receivedDateTime,from,bodyPreview, id, isRead, hasAttachments",
            "\$top" => "50"
        );
        $getMessagesUrl = '/me/mailfolders/inbox/messages?'.http_build_query($messageQueryParams);
        $messages = $graph->createRequest('GET', $getMessagesUrl)
            ->setReturnType(Model\Message::class)
            ->execute();

        $count = 0;
        foreach ($messages as $message) {
            if ($message->getIsRead() == false) {
                $count = $count + 1;
            }
        }
        return view('Office365.Mail.index', array(
            'messages' => $messages,
            'unread' => $count
        ));
    }

    public function mailCreate()
    {
        return (view('Office365.Mail.create'));
    }

    public function calendar()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $eventsQueryParams = array (
            "\$select" => "subject,start,end, id",
            "\$orderby" => "Start/DateTime DESC",
            "\$top" => "10"
        );

        $getEventsUrl = '/me/events?'.http_build_query($eventsQueryParams);
        $events = $graph->createRequest('GET', $getEventsUrl)
            ->setReturnType(Model\Event::class)
            ->execute();

        return view('Office365.Calendar.index', array(
            'events' => $events
        ));
    }

    public function calendarDetail($id)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $getEventUrl = '/me/events/'.$id;
        $event = $graph->createRequest('GET', $getEventUrl)
            ->setReturnType(Model\Event::class)
            ->execute();

        $eventBody = html_entity_decode($event->getBody()->getContent());

        return view('Office365.Calendar.detail', array(
            'event' => $event,
            'eventBody' => $eventBody
        ));
    }

    public function contacts()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $user = $graph->createRequest('GET', '/me')
            ->setReturnType(Model\User::class)
            ->execute();

        $contactsQueryParams = array (
            // // Only return givenName, surname, and emailAddresses fields
            "\$select" => "givenName,surname,emailAddresses",
            // Sort by given name
            "\$orderby" => "givenName ASC",
            // Return at most 10 results
            "\$top" => "10"
        );

        $getContactsUrl = '/me/contacts?'.http_build_query($contactsQueryParams);
        $contacts = $graph->createRequest('GET', $getContactsUrl)
            ->setReturnType(Model\Contact::class)
            ->execute();

        return view('Office365.Contact.index', array(
            'username' => $user->getDisplayName(),
            'contacts' => $contacts
        ));
    }
}
