<?php

namespace App\Http\Controllers\Youtube;

use Alaouy\Youtube\Youtube;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($country)
    {
       $videoList = \Youtube::getPopularVideos($country);
        return view('YouTube.index', array(
            'videos' => $videoList,
        ));
    }

    public function search(Request $search)
    {
        $videoList = \Youtube::search($search->input('search'));
        foreach ($videoList as $video) {
            if (isset($video->id->videoId)) {
                \Debugbar::addMessage($video->id->videoId);
            }
        }
        return view('YouTube.search', array(
            'videos' => $videoList,
        ));
    }

    public function getById($id)
    {
        $videoInfo = \Youtube::getVideoInfo($id);

        return $videoInfo;
    }
}
