<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    redirect('login');
});

Route::get('/about.json', 'AboutController@generateJsonFile');

Route::auth();
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('user', 'UserController');

Route::get('/office/mail', 'Office\OutlookController@mail')->name('mail');
Route::get('/office/mail/search/', 'Office\OutlookController@mailSearch')->name('mail.search');
Route::get('/office/mail/detail/{id}', 'Office\OutlookController@mailDetail')->name('mail.detail');
Route::get('/office/mail/new', 'Office\OutlookController@mailCreate')->name('mail.create');

Route::get('/office/calendar', 'Office\OutlookController@calendar')->name('calendar');
Route::get('/office/calendar/{id}', 'Office\OutlookController@calendarDetail')->name('calendar.detail');

Route::get('/office/contacts', 'Office\OutlookController@contacts')->name('contacts');

Route::get('/office/signin', 'Office\AuthController@signin');
Route::get('/office/authorize', 'Office\AuthController@getToken');

Route::get('/youtube/trends/{country}', 'Youtube\FlowController@index')->name('youtube');
Route::get('/youtube/search', 'Youtube\FlowController@search')->name('youtube.search');